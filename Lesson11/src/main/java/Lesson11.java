
import java.util.*;
import java.util.stream.Collectors;

public class Lesson11 {
    public static void main(String[] args) {

        List<String> subjects =  Arrays.asList("Noel", "The Cat", "The Dog");
        List<String> verbs =   Arrays.asList("wrote", "chased", "slept on");
        List<String> objects =  Arrays.asList("the book","the ball","the bed");


//      With regular for each loop
//        for (String s:subjects) {
//            for (String v: verbs) {
//                for (String o: objects ){
//                    System.out.println(s+" " +v + " "+ o);
//
//                }
//
               // with stream for each

//            subjects.stream().forEach(q->
//                    verbs.stream().forEach(w->
//                            objects.stream().forEach(e->
//                                    System.out.println(q+ " " + w+ " "+ e))));
//
//
//     with flatMap

        List<String> allComb =
                subjects.stream()
                        .flatMap(str1 -> verbs.stream().flatMap(str2->
                                objects.stream().map(str3 -> str1 +" "+  str2+" "+str3)))
                        .collect(Collectors.toList());

        System.out.println(allComb.toString());


        }


    }



