import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Lesson11Warmup2 {
    //
    public static void main(String[] args) {
       List< Integer> list = IntStream.generate(()-> (int) (Math.random()*20-10)).limit(20)
               .boxed()
               .collect(Collectors.toList());

        List< Integer> root = list.stream()
                .filter(x-> x>=0).map(x -> (int)( Math.sqrt(x)))
                .collect(Collectors.toList());

        List< Integer> negative_ones = list.stream()
                .filter(x -> x<0)
                .collect(Collectors.toList());


        System.out.println(list.toString());
        System.out.println(root.toString());
        System.out.println(negative_ones.toString());




    }
}
