
import java.util.*;
import java.util.stream.IntStream;

public class Hidden implements Iterable<String> {
        private final String[] months = {
                "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    List <Integer> numbers = new ArrayList<>();
    public Iterator<String> reverseIterator() {
        return new Iterator<String>() {
            int index = 11;

            @Override
            public boolean hasNext() {
                return index >= 0;
            }

            @Override
            public String next() {
                return months[index--];
            }
        };
    }
        @Override
        public Iterator<String> iterator() {

            return new Iterator<String>() {

                int index = 0;
                @Override
                public boolean hasNext() {
                    return index <12;
                }

                @Override
                public String next() {
                    index++;
                    return  getRandomMonth(RandomInt());
                }
            };

        }
        public  String getRandomMonth(int in){
            String month = months[in];
            numbers.add(in);
            return month;
        }
        public int RandomInt(){
            Random r = new Random();
         int ind = r.nextInt(12);
         while(numbers.contains(ind)) {
             ind = r.nextInt(12);


         }
             return ind;
         }


    }
