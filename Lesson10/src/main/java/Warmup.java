import java.io.InputStream;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Warmup {
    public static void main(String[] args) {
        String text = "Hello World";
        HashMap<Character, Integer> map = new HashMap<>();
        for (Character c : text.toCharArray()) {
            map.merge(c, 1, Integer::sum);
        }


        HashMap<Character, List<Integer>> map2 = new HashMap<>();
        for (int i = 0; i < text.length(); i++) {
            map2.merge(text.charAt(i), Arrays.asList(i + 1), new BiFunction<List<Integer>, List<Integer>, List<Integer>>() {
                @Override
                public List<Integer> apply(List<Integer> integers, List<Integer> integers2) {
                    List<Integer> result = new ArrayList<>();
                    result.addAll(integers);
                    result.addAll(integers2);
                    return result;
                }// 11111
            });
        }

        HashMap<Character, MyDataType> map3 = new HashMap<>();

        for (Character c : text.toCharArray()) {
            map3.merge(c, new MyDataType(map.get(c), map2.get(c)), new BiFunction<MyDataType, MyDataType, MyDataType>() {
                @Override
                public MyDataType apply(MyDataType myDataType, MyDataType myDataType2) {
                    MyDataType temp = new MyDataType();
                    temp.i = myDataType.i;
                    temp.list = new ArrayList<>();
                    temp.list.addAll(myDataType.list);
                    return temp;
                }

            });
        }




        Set<MyData> map4 = new HashSet<>();
        for (int i = 'a'; i <'z'; i++) {
            if(map.containsKey((char)(i))){
            map4.add(new MyData((char) i, map.get((char)(i)), map2.get((char)(i))));
        }}
        for (int i = 'A'; i <'Z'; i++) {
            if(map.containsKey((char)(i))){
                map4.add(new MyData((char) i, map.get((char)(i)), map2.get((char)(i))));
            }}


        System.out.println(map.toString());
        System.out.println(map2.toString());
        System.out.println(map3.toString());
        System.out.println(map4.toString());
    }
}
