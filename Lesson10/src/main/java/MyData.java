import java.util.HashSet;
import java.util.List;

public class MyData {
    Character c;
    Integer i ;
    List <Integer> list;

    public MyData() {
    }

    public MyData(Character c, Integer i, List<Integer> list) {
        this.c = c;
        this.i = i;
        this.list = list;
    }

    @Override
    public String toString() {
        return
                 c +
                " " + i +
                " " + list
                ;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return  false;
        if (!(obj instanceof MyData )) return false;
        MyData m = (MyData) (obj);
        return this.c == m.c &&
                this.i == m.i &&
                this.list == m.list;
    }
//    public HashSet<MyData> sort (HashSet<MyData> myset){
//        for (int j = 1; j < myset.size(); j++) {
//           myset.forEach;
//
//            }
//        }
    }

