import java.util.Optional;

public class Lesson12 {
    public static void main(String[] args) {
        Optional value1 = strToInt("123");
        Optional value2 = strToInt("12s");
        System.out.println(value1);
        System.out.println(value2);

    }

    public static Optional < Integer> strToInt(String s){
        Optional <Integer> value;
        try {
            int x = Integer.parseInt(s);
            value = Optional.of(x);
        }
        catch (Exception e){
            value = Optional.empty();
        }
        return value;


    }
}
