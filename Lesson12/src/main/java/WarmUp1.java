import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WarmUp1 {
    public static void main(String[] args) throws IOException {
        HashMap<String, List<String>> subj_verb = new HashMap<>();
        String path = "subj_verb.txt";

        try (
                BufferedReader br1 = new BufferedReader(new FileReader(path));

        ) {
            Stream<String> lines = br1.lines();

            List<String[]> lin = lines.map(x -> x.split(":")).collect(Collectors.toList());
            for (String[] l : lin) {
                subj_verb.put(l[0], Arrays.asList(l[1].split(",")));
            }
            System.out.println(subj_verb.toString());

        }

        path = "verb_obj.txt";
        HashMap<String, List<String>> verb_obj = new HashMap<>();
        try (
                BufferedReader br1 = new BufferedReader(new FileReader(path));

        ) {
            Stream<String> lines = br1.lines();

            List<String[]> lin = lines.map(x -> x.split(":")).collect(Collectors.toList());
            for (String[] l : lin) {
                verb_obj.put(l[0], Arrays.asList(l[1].split(",")));
            }
            System.out.println(verb_obj.toString());

        }


        List<String> collect = subj_verb.keySet().stream()
                .flatMap(subject -> subj_verb.get(subject.trim()).stream()
                        .flatMap(verb -> verb_obj.get(verb.trim()).stream()
                                .map(object -> subject + " " + verb + " " + object)))
                .collect(Collectors.toList());


        System.out.println(collect.toString());

    }
}


