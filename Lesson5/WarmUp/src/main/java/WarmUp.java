import java.util.ArrayList;
import java.util.Random;

public class WarmUp {
    public static void main(String[] args) {

        ArrayList <Figure> figures = new ArrayList<>();
        for (int i = 0; i < new Figure().generateRandom(); i++) {
            figures.add(generateFigure());
        }

        double total = 0;


         for (Figure fig:figures) {
            total += fig.area();
           }


        System.out.println(total);
    }

    public static Figure generateFigure(){
        Random random = new Random();
        int x = random.nextInt(3-1)+1;
         if (x ==1) return Circle.generateCircle();
         if (x==2) return  Rectangle.generateRectangle();
          return Triangle .generateTriangle();
}

}
