import java.util.Random;

public class Figure {


    public double area(){
        throw new IllegalArgumentException("Doesn't return anything");
    };


    public static Point generatePoint(){
        return new Point(generateRandom(), generateRandom());

    }

    public static int generateRandom(){
        Random random = new Random();
        return  random.nextInt(10-1)+1;
    }


}
