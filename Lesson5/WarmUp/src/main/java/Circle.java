public class Circle extends Figure{
    Point p1;
    int radius;

    public Circle(Point p1, int radius) {
        this.p1 = p1;
        this.radius = radius;
    }

    @Override
    public double area() {
        return Math.PI*Math.pow(radius,2);
    }

    public static Circle generateCircle(){
        return new Circle(generatePoint(), generateRandom());
    }
}
