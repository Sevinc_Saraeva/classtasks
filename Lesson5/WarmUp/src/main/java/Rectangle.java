public class Rectangle extends Figure {
Point p1;
Point p2;
Point p3;
Point p4;

    public Rectangle(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }


    @Override
    public String toString() {
        return "Rectangle{" +
                "p1=" + p1.toString() +
                ", p2=" + p2.toString() +
                '}';
    }

    @Override
    public double area() {
       return (p1.y-p2.y)*(p2.x-p1.x);
    }



    public static Rectangle generateRectangle(){
        return new Rectangle( generatePoint(), generatePoint());
    }
}
