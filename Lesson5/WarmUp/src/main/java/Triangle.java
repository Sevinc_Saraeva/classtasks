public  class Triangle extends Figure{
    Point p1;
    Point p2;
    Point p3;

    public Triangle(Point p1, Point p2, Point p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }


    @Override
    public double area() {
       double dist1 = Math.sqrt(Math.pow((p1.x-p2.x), 2) + Math.pow((p1.y-p2.y),2));
        double dist2 = Math.sqrt(Math.pow((p1.x-p3.x), 2) + Math.pow((p1.y-p3.y),2));
        return (double)(dist1*dist2/2);


    }
    public static Triangle generateTriangle(){
        return new Triangle(generatePoint(), generatePoint(), generatePoint());
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "p1=" + p1.toString() +
                ", p2=" + p2.toString() +
                ", p3=" + p3.toString() +
                '}';
    }
}
