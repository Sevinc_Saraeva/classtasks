import java.util.*;

public class Lesson8 {
    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        HashSet<Integer> set = new HashSet<>();

        for (int i = 0; i < 30; i++) {
            int k = (int) (Math.random() * 20) - 10;
            list.add(k);
            set.add(k);
        }
  //list.stream().sorted(a,b -> a);

        System.out.println(list.toString());
        System.out.println("----------");
        System.out.println(set.toString());
        System.out.println();


        List <String> names = new ArrayList<>();
        List <String> namesRandom = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
          names.add(generateName());
        }


        for (int i = 0; i < 10; i++) {
            namesRandom.add(names.get((int)(Math.random()*20)));
        }

        System.out.println(names.toString());
        System.out.println(namesRandom.toString());

    }



     public static String generateName() {
         StringBuilder sb = new StringBuilder();
         for (int j = 0; j < 5; j++) {
             sb.append(randomSmallLetter());
         }
         return sb.toString();
     }


        public static  char randomSmallLetter() {
            String alpha = alphabetSmall();
            return alpha.charAt((int) (Math.random()*alpha.length()));
        }

    public static String alphabetSmall() {
        StringBuilder alpha = new StringBuilder();
        for (char i = 'a'; i <= 'z'; i++) {
            alpha.append(i);
        }
        return alpha.toString();
    }
}
