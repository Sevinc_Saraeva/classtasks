import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Predicate;

public class Task2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please, enter the length of array");
        int SIZE = in.nextInt();
        int[] ar = getRandom(SIZE);
        System.out.println(Arrays.toString(ar));
        int[]shiftedPozitive = shiftPozitive(ar);
      int [] shifthedNegative = shiftNegative(shiftedPozitive);


       System.out.println(Arrays.toString(shiftedPozitive));
        //System.out.println(Arrays.toString(shifthedNegative));
    }

    private static int[] getRandom(int size) {
        int[] ar = new int[size];
        for (int i = 0; i < size; i++) {
            ar[i] = (int) (Math.random() * 200) - 100;
        }
        return ar;
    }

    private static int[] shiftPozitive(int[] ar) {
        for (int i = 0; i < ar.length - 1; i++) {
            if (ar[i] > 0) {
              int x = searchPozitiveNumber(ar, i+1, ar.length);
               if(x!=-1){
                swap(ar, i, x);
}
              }

        }
        return ar;
    }

    public static  int [] shiftNegative(int[] ar){
        for (int i = 1; i <ar.length ; i++) {
           int x = searchNegativeNumber(ar,i-1);
                   if(x!=-1)
                       swap(ar, i,x);
               }
        return ar;
            }




    public static int searchPozitiveNumber(int[] ar, int begin, int end){
        for (int i = begin; i <end; i++) {
         if(ar[i]>0){
             return i;
           }
}
        return -1;
    }

    public static int searchNegativeNumber(int[] ar, int begin){
        for (int i = begin; i >=0; i--) {
            if(ar[i]<0){
                return i;
            }
        }
        return -1;
    }

    public  static void swap(int[]ar, int i, int j){
        int temp = ar[i];
        ar[i] = ar[j];
        ar[j] = temp;
           }

}