import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.function.Predicate;

public class Lesson5 {
    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in );
        System.out.println("Please, enter N: ");
        int N = in.nextInt();

        int [] odd = fillArray(N, x-> x%2!=0);
        int [] even =  fillArray(N, x-> x%2==0);
        int[] mixed = fillMixedArray(odd, even);

        System.out.println("Odd array: "+ Arrays.toString(odd));
        System.out.println("even array: " + Arrays.toString(even));
        System.out.println("mixed array: "+Arrays.toString(mixed));
    }

    private static int[] fillArray (int length, Predicate <Integer> p){

        int [] arr = new int[length];
        int index = 0;
        while(index<length){
            int x = getRandom();
            if(p.test(x)) {
                arr[index] = x;
                index++;
            }
        }
        return arr;
    }
    private static int getRandom(){
        Random random  =new Random();
        return random.nextInt(100-2+1)+2;
    }
    private static int[]  fillMixedArray(int[]odd, int [] even){
        int [] mixed =new int[odd.length*2];
        int index= 0;

        for (int i = 0; i <odd.length; i++) {
            mixed[index] = odd[i];
            mixed[index+1] = even[i];
            index+=2;

        }
        return mixed;
    }





}
