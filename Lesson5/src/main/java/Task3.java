import java.util.Arrays;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int width = in.nextInt();
        int height = in.nextInt();
        int [][] ar = new int[width][height];

        for (int i = 0; i < width; i++) {
             for (int j = 0; j < height; j++) {
                    if (i == 0 || j == 0 || i == width - 1 || j == height - 1) ar[i][j] = 1;

            }
            for (int j = 1; j <height/2 ; j++) {
                if (i != 0 && i != width - 1) {
                    ar[j][i] =j+1;
                    ar[height - 1 - j][i] =j+1;
 }

            }
        }
        for (int i = 0; i < height; i++) {
            for (int j = 0; j <width ; j++) {
                System.out.print(ar[i][j] + " ");
            }
            System.out.println();
        }
    }
}
