package Task3;

public class Formatter1 extends Formatter0 {
    String str;

    public Formatter1(String str) {
        this.str = str;

    }
    public Formatter1(Formatter0 frmt) {
        this.str = frmt.toString();

    }

    @Override
    public String toString() {
        return str.toLowerCase();
    }
}
