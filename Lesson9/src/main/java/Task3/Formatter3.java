package Task3;

public class Formatter3 extends Formatter0 {
    String str;
    public Formatter3(String str) {
        this.str = str;
    }

    public String toString() {
        return  "***********\n" +
                "* " +  str + "*\n" +
                "***********\n";
    }

}


